module.exports = (router, stripe) => {
  router.get('/payments/clientSecret', async function (req, res, next) {
    try {
      let paymentIntent = await stripe.paymentIntents.create({
        amount: 5 * 100,
        currency: 'eur',
      });
      const clientSecret = paymentIntent.client_secret;
      res.send({
        data: {clientSecret}
      })
    } catch (error) {
      res.status(500).json({message: error.message});
    }
  });
}
