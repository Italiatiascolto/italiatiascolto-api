const express = require('express');
const router = express.Router();

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config();
}

const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

/* PAYMENTS */
require('./payments')(router, stripe);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
