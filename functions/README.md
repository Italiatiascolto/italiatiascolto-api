# solviendo-be-functions

## Develop

### Environment variables

Sync enviroment variables locally

```shell
firebase functions:config.get > .runtimeconfig.json
```

Set env

```shell
firebase functions:config:set foo.bar=10
```

Run local servers

```shell
npm run serve
```
