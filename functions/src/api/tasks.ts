import * as admin from "firebase-admin";
import { region } from "firebase-functions";

export const get = region('europe-west1').https.onRequest(async (req, res) => {
    const [, taskId, format] = req.path.match(/\/(\w+)\.?(json|)/) || [];
    if (!taskId) {
        res.status(404).end();
        return;
    }

    const task = await admin.firestore()
        .collection("tasks")
        .doc(taskId)
        .get();

    if (!task.exists) {
        res.status(404).end();
        return;
    }
    
    const when: string = task.get('when').toDate().toISOString();
    const where: string = task.get('where');
    const price: Number = task.get('price');
    const metadata: {
        name: string,
        description: string,
        title: string,
        image: string,
        provider: string,
        category: string,
    } = task.get('meta');

    if (!format) {
        res.send(`
            <html lang="en">
                <head>
                    <title>${ metadata.name}</title>
                    <meta name="description" content="${ metadata.description }" />
                    <meta name="author" content="${ metadata.provider}" />
                    <meta name="where" content="${ where }" />
                    <meta name="when" content="${ when }" />
                    <meta name="price" content="${ price }" />
                    <meta property="og:title" content="${ metadata.title }" />
                    <meta property="og:description" content="${ metadata.description }" />
                    <meta property="og:image" content="${ metadata.image }" />
                    <script type="application/ld+json">
                    {
                        "@context": "https://schema.org/",
                        "@type": "Product",
                        "name": "${ metadata.name } ",
                        "image": [ "${ metadata.image } "],
                        "description": "${ metadata.description }",
                        "sku": "${task.id}",
                        "brand": "${ metadata.category}",
                        "additionalProperty": [
                            {
                                "@type": "PropertyValue",
                                "name": "when",
                                "value": "${ when }"
                            },
                            {
                                "@type": "PropertyValue",
                                "name": "where",
                                "value": "${ where }"
                            }
                        ],
                        "offers": {
                            "@type": "Offer",
                            "availability": "http://schema.org/InStock",
                            "price": "${ price }",
                            "priceCurrency": "EUR",
                            "priceValidUntil": "${ when }",
                            "seller": {
                                "@type": "Person",
                                "name": "${ metadata.provider}"
                            }
                        } 
                    }
                    </script>
                </head>
            </html>
        `);
    } else if (format === 'json') {
        res.json({
            title: metadata.name,
            description: metadata.description,
            image: metadata.image,
            author: metadata.provider,
            category: metadata.category,
            where,
            when,
            price,
        })  
    }
});
