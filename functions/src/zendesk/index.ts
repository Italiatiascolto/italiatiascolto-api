import { region, config } from "firebase-functions";
import * as admin from "firebase-admin";
import * as cors from "cors";
import { v4 as uuid } from "uuid";
import { sign } from 'jsonwebtoken';

type metadataType = {
    idToken?: string,
    sub?: string
};

export const sso = region('europe-west1').https.onRequest((req, res) =>
    cors({ origin: true })(req, res, async () => {        
        let metadata: metadataType = {};
        if (!req.headers.authorization || !req.headers.authorization.startsWith("Bearer ")) {
            res.status(403).json({ metadata, error: { code: 403, message: "Unauthorized" } });
            return;
        }
        const idToken = req.headers.authorization.split('Bearer ')[1];
        metadata = { ...metadata, idToken };
        try {
            const decodedIdToken = await admin.auth().verifyIdToken(idToken);
            metadata = { ...metadata, sub: decodedIdToken.sub };
            const jwt = sign({
                email: "fabio.melen+solviendo@gmail.com", name: "Fabio Melen",
            }, config().zendesk.secret, {
                jwtid: uuid(),
            });
            res.json({ metadata, data: { jwt } });
        } catch(e) {
            res.status(500).json({ metadata, error: { code: 500, message: e.message } });
        }
    }));
