import { region } from 'firebase-functions';

export const ping = region('europe-west1').https.onRequest((req, res) => {
    res.send('Hello');
});
